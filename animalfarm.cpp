#include <iostream>
#include <mpi.h>
#include <unistd.h>
#define MCW MPI_COMM_WORLD

void pig( int size ) {
  int workToDo;
  for( int i=1; i<size; i++ ) {
    workToDo = rand()%10 + 1;
    std::cout << "The pig is directing horse " << i << " to do " << workToDo << " units of work." << std::endl;
    MPI_Send( &workToDo, 1, MPI_INT, i, 0, MCW );
  }
  int horseStatus;
  MPI_Status msgInfo;
  int horseCount = size-1;
  while( horseCount > 0 ) {
    MPI_Recv( &horseStatus, 1, MPI_INT, MPI_ANY_SOURCE, 0, MCW, &msgInfo );
    if( !horseStatus ) {
      std::cout << "Horse " << msgInfo.MPI_SOURCE << " has sacrificed his bones and hooves for the greater good." << std::endl;
      horseCount--;
      std::cout << horseCount << " horses left" << std::endl;
    } else {
      workToDo = rand()%10 + 1;
      std::cout << "Horse " << msgInfo.MPI_SOURCE << " lives to do " << workToDo << " units of work." << std::endl;
      MPI_Send( &workToDo, 1, MPI_INT, msgInfo.MPI_SOURCE, 0, MCW );
    }
  }
}

void horse( int rank ) {
  int workToDo;
  int life = 1;
  while( life > 0 ) {
    MPI_Recv( &workToDo, 1, MPI_INT, 0, 0, MCW, MPI_STATUS_IGNORE );
    std::cout << "Horse " << rank << " working." << std::endl;
    sleep( workToDo );
    std::cout << "horse " << rank << " very tired." << std::endl;
    life = rand()%5!=0;
    MPI_Send( &life, 1, MPI_INT, 0, 0, MCW );
  }
}

int main(int argc, char **argv){

  int rank, size;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MCW, &rank);
  MPI_Comm_size(MCW, &size);
 
  if( rank==0 ) {//I am the pig
    pig( size );
  } else {//I am a horse
    horse( rank );
  }

  MPI_Finalize();
  return 0;
}
				
