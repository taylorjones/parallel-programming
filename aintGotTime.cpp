#include <iostream>
#include <mpi.h>
#include <unistd.h>
#define MCW MPI_COMM_WORLD

void master( int size ) {
  sleep(10);
  int data = 42;
  MPI_Send(&data, 1, MPI_INT, 1, 0, MCW);
}

void slave( int rank ) {
  int data;
  MPI_Request msgRequest;
  MPI_Status msgStatus;
  MPI_Irecv(&data, 1, MPI_INT, 0, 0, MCW, &msgRequest);
  int received = 0;
  while( !received ) {
    std::cout << "Waiting...\n" << std::endl;
    MPI_Test(&msgRequest, &received, &msgStatus);
    sleep(1);
  }
  std::cout << "Process " << rank << " received a " << data << std::endl;
}

int main(int argc, char **argv){

  int rank, size;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MCW, &rank);
  MPI_Comm_size(MCW, &size);
 
  if( rank==0 ) {//I am the pig
    master( size );
  } else {//I am a horse
    slave( rank );
  }

  MPI_Finalize();
  return 0;
}
				
