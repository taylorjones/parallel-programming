#include <iostream>
#include <mpi.h>
#include <unistd.h>
#include <cmath>
#include <algorithm>
#define MCW MPI_COMM_WORLD

int rank, size;

int cube(int i, int data){
    int dest = rank ^ (1<<i);
    MPI_Send(&data,1,MPI_INT,dest,0,MCW);
    MPI_Recv(&data,1,MPI_INT,dest,0,MCW,MPI_STATUS_IGNORE);
    return data;

}

void print1per(int data){
  int *dArray = new int[size];
  MPI_Gather(&data,1,MPI_INT,dArray,1,MPI_INT,0,MCW);
  if(rank==0){
    for(int i=0;i<size;++i){
      std::cout << i << ":";
      std::cout << dArray[i] << " ";
    }
    std::cout << std::endl;
  }
}


int main(int argc, char **argv){

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MCW, &rank);
  MPI_Comm_size(MCW, &size);
  srand(size*rank+1); 
//  MPI_Send(&rank,1,MPI_INT,(rank+1)%size,0,MCW);
//  MPI_Recv(&data,1,MPI_INT,MPI_ANY_SOURCE,0,MCW,MPI_STATUS_IGNORE);
  int isDesc  = 0;
  int num = rand() % 9999;
  int temp, desc, right, keepMax, i, j;
  int bits = log2(size);
  if(std::pow(2,bits)!=size) {
    if(rank==0)
      std::cerr << "Number of processors must be n=2^k." << std::endl;
    MPI_Finalize();
    return 0;
  }
  for(i=0;i<bits;++i) {
    for(j=i;j>=0;--j) {
      desc = (rank>>(i+1)) & 1; //Check i+1 bit for descending (descending if out of bits)
      right = (rank>>(j)) & 1;  //Check j bit for right or left
      keepMax = desc^right^isDesc; //Combine overall Ascending with local descending/right
      temp = cube(j,num);
      if(keepMax)
        num = std::max(num,temp);
      else
        num = std::min(num,temp);
      MPI_Barrier(MCW);
    }
  }
  print1per(num);
  MPI_Finalize();
  return 0;
}
				
