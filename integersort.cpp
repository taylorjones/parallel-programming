#include <iostream>
#include <mpi.h>
#include <algorithm>
#define MCW MPI_COMM_WORLD

void merge(int * destination, int * source1, int src1Count, int * source2, int src2Count ) {
  int src1I = 0;
  int src2I = 0;
  for(int i=0;i < src1Count+src2Count;i++) {
    if(src1I > src1Count || (src2I < src2Count && source1[src1I] >= source2[src2I])) {
      destination[i] = source2[src2I];
      src2I++;
    }
    else {
      destination[i] = source1[src1I];
      src1I++;
    }
  }
}

void master( int size ) {
  int length = (rand()%5+5)*(size-1);
  int* nums = new int[length];
  int* dest = new int[length];
  int* data = new int[length/(size-1)];
  for(int i = 0;i < length; i++) {
    nums[i] = rand()%89+10;
    std::cout << nums[i] << " ";
  }
  std::cout << std::endl;
  for(int i=1; i<size; i++) {
    MPI_Send(nums+((i-1)*length/(size-1)),length/(size-1),MPI_INT,i,0,MCW);
  }
  std::cout << "Sent arrays" << std::endl;
  int count = 0;
  for(int i=1; i<size; i++) {
    MPI_Recv(data,length/(size-1),MPI_INT,MPI_ANY_SOURCE,0,MCW,MPI_STATUS_IGNORE); 
    merge(nums, data, length/(size-1), dest, count);
    count += length/(size-1);
    std::swap(nums,dest); 
  }
  for(int i = 0;i < length; i++) {
    std::cout << dest[i] << " ";
  }
  std::cout << std::endl << "Done sorting" << std::endl;
}

void slave( int rank ) {
  MPI_Status status;
  MPI_Probe(0, 0, MPI_COMM_WORLD, &status);
  int count;
  MPI_Get_count(&status, MPI_INT, &count);
  int *nums = new int[count];
  MPI_Recv(nums,count,MPI_INT,0,0,MCW,MPI_STATUS_IGNORE);
  std::cout << rank << " Sorting " << count << ":";
  for(int i=0; i<count; i++) {
    std::cout << nums[i] << " ";
  }
  std::cout << std::endl;
  std::sort(nums,nums+count);
  std::cout << rank << " Sorted " << count << ":";
  for(int i=0; i<count; i++) {
    std::cout << nums[i] << " ";
  }
  std::cout << std::endl;
  MPI_Send(nums,count,MPI_INT,0,0,MCW);
}

int main(int argc, char **argv){

  int rank, size;
  int data;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MCW, &rank);
  MPI_Comm_size(MCW, &size);

  if(rank == 0)
    master( size );
  else
    slave( rank );

  MPI_Finalize();
  return 0;
}
				
