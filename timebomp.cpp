#include <iostream>
#include <mpi.h>
#define MCW MPI_COMM_WORLD

int main(int argc, char **argv){

  int rank, size;
  int data;
  int potatoTimer = rand();
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MCW, &rank);
  MPI_Comm_size(MCW, &size);
  
  bool running = true;

  if(rank==0) {  
    MPI_Send(&potatoTimer,1,MPI_INT,rand()%size,0,MCW);
  }
  
  while(running) {
    MPI_Recv(&potatoTimer,1,MPI_INT,MPI_ANY_SOURCE,0,MCW,MPI_STATUS_IGNORE);
    if(potatoTimer == 0) {
      std::cout << "#" << rank << " all done" << std::endl;
      running = false;
    } else {
      potatoTimer--;
      std::cout << "I am " << rank << ". " << "Bomb is " << potatoTimer << "." << std::endl;
      if(potatoTimer > 0) {
        MPI_Send(&potatoTimer,1,MPI_INT,rand()%size,0,MCW);
      } else {
        std::cout << "I am " << rank << ". I lost." << std::endl;
        for(int i = 0;i < size; i++) {
          MPI_Send(&potatoTimer,1,MPI_INT,i,0,MCW);
        }
      }
    }
  }

  MPI_Finalize();
  return 0;
}
				
