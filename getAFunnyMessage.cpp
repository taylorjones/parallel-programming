
#include <iostream>
#include <mpi.h>
#include <unistd.h>
#define MCW MPI_COMM_WORLD

void master( int size ) {
  int msgTag = rand()%5;
  int msgLength = rand()%100;
  int * msgBuf = new int[msgLength];
  for(int i = 0; i < msgLength; i++) {
    msgBuf[i] = i;
  }
  std::cout << "Master sending a message tag:" << msgTag << " and length:" << msgLength << "." << std::endl;
  MPI_Send(msgBuf,msgLength,MPI_INT,1,msgTag,MCW);
}

void slave( int rank ) {
  int msgSize;
  MPI_Status msgStatus;
  MPI_Probe(MPI_ANY_SOURCE,MPI_ANY_TAG,MCW,&msgStatus);
  MPI_Get_count(&msgStatus, MPI_INT, &msgSize);
  std::cout << "Recieving message from " << msgStatus.MPI_SOURCE << " with tag " << msgStatus.MPI_TAG << " with size " << msgSize << "." << std::endl;
}

int main(int argc, char **argv){

  int rank, size;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MCW, &rank);
  MPI_Comm_size(MCW, &size);
 
  if( rank==0 ) {//I am the pig
    master( size );
  } else {//I am a horse
    slave( rank );
  }

  MPI_Finalize();
  return 0;
}
				
